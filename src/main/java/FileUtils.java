import java.io.IOException;
import java.nio.file.Path;

public class FileUtils {

    private FileType fileType;
    protected Path filePath;

    public FileUtils() {
    }

    public FileUtils(FileType fileType, Path filePath) {
        this.fileType = fileType;
        this.filePath = filePath;
    }

    public void factory() throws IOException {

        switch(fileType){
            case TXT:
                TxtFileCrud txtFileCrud = new TxtFileCrud();
                txtFileCrud.CreateFile();
                break;

            case JAVA:
                JavaFileTypeCrud javaFileTypeCrud = new JavaFileTypeCrud();
                javaFileTypeCrud.CreateFile();
                break;

            case XML:
                XmlFileTypeCrud xmlFileTypeCrud = new XmlFileTypeCrud();
                xmlFileTypeCrud.CreateFile();
                break;


        }

    }


}
