import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TxtFileCrud extends FileUtils implements FileOperations  {

    @Override
    public void CreateFile() throws IOException{
      Files.createFile(filePath);
    }


    @Override
    public void readFile() throws IOException {
       try(BufferedReader br = new BufferedReader(new FileReader(String.valueOf(filePath)))){
        int data;
        while ((data=br.read())!=-1){
            System.out.print((char)data);
        }
       }catch (Exception e){
           e.printStackTrace();
       }
    }

    @Override
    public void write2File() throws IOException {



    }
    @Override
    public void deleteFile() throws IOException{

    }

    @Override
    public void changeFilePermissions() {

    }

    @Override
    public void CopyFileContents() {

    }

}
